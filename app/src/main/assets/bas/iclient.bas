' X11-Basic example of how to use sockets
' This Program acts as a client to a web-server on Port 80

' print @getpagecontent$("x11-basic.sourceforge.net","/index.html")

' if you want to connect to the iserver.bas on localhost, do a

OPEN "UC",#1,"localhost",5510
PRINT #1,"TIME"+CHR$(13)
FLUSH #1
LINEINPUT #1,response$
PRINT "Response: ";response$
LINEINPUT #1,response$
PRINT "Response: ";response$
PRINT #1,"QUIT"+CHR$(13)
FLUSH #1
LINEINPUT #1,response$
PRINT "Response: ";response$
CLOSE #1
END

' Get a whole WEB Page form a server

FUNCTION getpagecontent$(server$,page$)
  LOCAL a$,b$,t$
  ret$=""
  OPEN "UC",#1,server$,80
  PRINT #1,"GET "+page$+" HTTP/1.0"+CHR$(13)
  PRINT #1,"Host: "+server$+CHR$(13)
  PRINT #1,"User-Agent: X11-Basic/1.12"+CHR$(13)
  PRINT #1,CHR$(13)
  FLUSH #1
  LINEINPUT #1,response$
  SPLIT response$," ",0,protocol$,response$
  SPLIT response$," ",0,htmlerror$,response$
  ' print "Response: ";response$
  IF val(htmlerror$)=200
    ' Parse Header
    LINEINPUT #1,t$
    t$=REPLACE$(t$,CHR$(13),"")
    WHILE len(t$)
      '  print t$,len(t$)
      SPLIT t$,":",0,a$,b$
      IF a$="Content-Length"
        length=VAL(b$)
      ENDIF
      LINEINPUT #1,t$
      t$=REPLACE$(t$,CHR$(13),"")
    WEND
    ' print "Len=";length;" Bytes."
    IF length
      ret$=input$(#1,length)
    ENDIF
  ELSE
    PRINT "Response: ";response$
    PRINT "Error: could not get data from the server!"
  ENDIF
  CLOSE #1
  RETURN replace$(ret$,CHR$(13),"")
ENDFUNCTION
