'
' X11-Basic example how to run a server on port 5510
' which accepts connections from clients.
'
'
' Run this program on a host machine
'
' To test it, use
' telnet <hostname-or-ip-address> 5510
'
' (c) Markus Hoffmann 2005-20012
'
'
port=5510

' Make a pipe, so that the different forks can communicate with each other
PIPE #10,#11

OPEN "US",#1,"",port
PRINT "Listening on port ";port;" for 5 Minutes."
FLUSH
t=TIMER
AFTER 250,shutdown
DO
  count=freefile()
  PRINT "Waiting for connection on channel ";count;" ..."
  ' The following command will block until a client connects.
  IF INP?(#10)
    LINEINPUT #10,t$
  ENDIF
  EXIT IF t$="SHUT"

  OPEN "UA",#count,"",1
  a=FORK()
  IF a=0
    @handle_connection(count)
    PRINT "Connection #";count;" closed."
    CLOSE #count
    QUIT
  ELSE
    PRINT "Spawned process ";a
  ENDIF
  EXIT IF TIMER-t>300
LOOP
CLOSE #1
PRINT "Server has stopped. Will not accept new connections."
END ! if you say quit here, all running connections will be interrupted.
QUIT
PROCEDURE shutdown
  PRINT "...timeout..."
  PRINT #11,"SHUT"
  FLUSH #11
  PRINT "Server has stopped."
RETURN
PROCEDURE handle_connection(c)
  ' c holds the file handle of the channel to the client
  LOCAL t$
  PRINT #c,"Welcome to X11-Basic test-server ...PID=";a;", count=";c
  FLUSH #c
  DO
    t$=""
    IF INP?(#c)
      LINEINPUT #c,t$
      PRINT "Connection #"+STR$(c)+": got: ";t$
      FLUSH
    ENDIF
    t$=UPPER$(LEFT$(t$,4))
    EXIT IF t$="QUIT"
    IF t$="QUIT"
      BREAK
    ELSE IF t$="SHUT"
      PRINT #11,"SHUT"
      FLUSH #11
      BREAK
    ELSE IF t$="HELP"
      PRINT #c,"You can send QUIT to close the connection."
      PRINT #c,"You can send SHUT to shutdown the server."
      PRINT #c,"You can send TIME to ask for the TIME."
      FLUSH #c
    ELSE IF t$="DUMP"
      DUMP
      DUMP "#"
    ELSE IF t$="TIME"
      PRINT #c,time$
      FLUSH #c
    ELSE IF LEN(t$)
      PRINT #c,"I do not understand the command: "+t$
      FLUSH #c
    ENDIF
  LOOP
  PRINT #c,"goodbye..."
  FLUSH #c
RETURN
