'
'	Bresenham's algorithm for drawing line
'
' Ported to X11-Basic (c) Markus Hoffmann 2003
'
phi=0
DO
  CLS
  PRINT AT(ROWS/2-1,COLS/2-20);"Bresenham X11-Basic (c) Markus Hoffmann 2003"
  xx=2*(11+10*cos(phi1/180*pi))
  xxx=2*(COLS/4-19*cos(phi2/180*pi))
  yy=ROWS/2+(ROWS/2-1)*sin(phi1/180*pi)
  yyy=ROWS/2-(ROWS/2-1)*sin(phi2/180*pi)
  @gline(xx,yy,xxx,yyy)
  FLUSH
  PAUSE 0.05
  ADD phi1,10
  ADD phi2,13
LOOP
PRINT
END
PROCEDURE PLOT(x,y)
  PRINT AT(y,x);"#";
RETURN

PROCEDURE gline(x1,y1,x2,y2)
  dX=x2-x1
  dY=y2-y1
  pos_slope=abs(dX>0)
  IF dY<0
    pos_slope=1-pos_slope
  ENDIF
  IF ABS(dX)>ABS(dY)
    IF dX>0
      col=x1
      row=y1
      final=x2
    ELSE
      col=x2
      row=y2
      final=x1
    ENDIF
    inc1=2*ABS(dY)
    G=inc1-ABS(dX)
    inc2=2*(ABS(dY)-ABS(dX))
    IF pos_slope
      WHILE col<=final
        @plot(col,row)
        INC col
        IF G>=0
          INC row
          ADD G,inc2
        ELSE
          ADD G,inc1
        ENDIF
      WEND
    ELSE
      WHILE col<=final
        @plot(col,row)
        INC col
        IF G>0
          DEC row
          ADD G,inc2
        ELSE
          ADD G,inc1
        ENDIF
      WEND
    ENDIF
  ELSE
    IF dY>0
      col=x1
      row=y1
      final=y2
    ELSE
      col=x2
      row=y2
      final=y1
    ENDIF
    inc1=2*ABS(dX)
    G=inc1-ABS(dY)
    inc2=2*(ABS(dX)-ABS(dY))
    IF pos_slope
      WHILE row<=final
        @plot(col,row)
        INC row
        IF G>=0
          INC col
          ADD G,inc2
        ELSE
          ADD G,inc1
        ENDIF
      WEND
    ELSE
      WHILE row<=final
        @plot(col,row)
        INC row
        IF G>0
          DEC col
          ADD G,inc2
        ELSE
          ADD G,inc1
        ENDIF
      WEND
    ENDIF
  ENDIF
RETURN
