' Plays ATARI ST XBIOS(32) Sound files (.X32) on the speaker
'
' (c) Markus Hoffmann 2003
' adapted to modified sound command 2011
'
' do a:
' xbasic playsnd.bas popcorn.snd

speed=0.25

' This is an original ATARI XBIOS(32) Sound file
' popcorn.snd 17024 Bytes. (compressed: 1123 Bytes, 6%)
popcorn$=""
popcorn$=popcorn$+";_1]62.NFF+W&73[1;N;4CTSBDF[6T;JS9.7K2@.aGZWAL5S7%VG7(bR`^1FU'?Z"
popcorn$=popcorn$+"Z$_JRYNW6?B(BY;b`](DYR+6P'@$C(aV*P%.DX(=)AH6VV><+M^$BFR*97')&W.6"
popcorn$=popcorn$+"O6L>JB+%33Z_<UEZXU\(GLS0:Z7RBXMSFZ<R?P9,*G(CBcU>=R4b.LWJC48D(I4+"
popcorn$=popcorn$+"RI)$=^Q2B$A]Y3HG/4XH)?6NOYAKP<Y/`VY_&5,8LcB=5H0P>W%UKR=KS>(O3O>a"
popcorn$=popcorn$+"'b8L15['cK5F*?N(R_aa6'I<>SC>JU`0c2H5_T;?4QZBa%S2`OBO'b]_L$\H%7FW"
popcorn$=popcorn$+"GSEY`A0%bWD/)%G;'R,B,.59[FTA34DIDHK=*>$&([X)\-`F;4+E,b1R(9Y$/<G&"
popcorn$=popcorn$+":B4'`&4(@T7,cA^C/FM2Q2\S'DPQ?ODL'6a^c+=YMWF@]%,F%LBB=-[,6=RDH3'["
popcorn$=popcorn$+"2NF'+/HS?&)^G3PQJL26<Y@J/ZRFB2=)0Oa*KJ_YFQ&L;B>*I:cc1bLGH)$P$&6R"
popcorn$=popcorn$+"GB5_=PJDM5`D?_USC_2/bLZ8KX$5[*XH(@?[[-K+JB--6VRObRa]UC@S,I@HGB4a"
popcorn$=popcorn$+"BLKTK_2&5O;)a;`+&B^YQR?:;I'(GDFaH%^6Q/S:IPL7K3U=V;0+34JMJ:UD]<83"
popcorn$=popcorn$+"B?`5=:8UVJS;5(J$AONN\/1b+@VFF?-U?-R.$Z]EE4B]\\6+'2_GW(Y`/B?X$:R)"
popcorn$=popcorn$+"$IVJ%OXcIM[2aT9GcZ1a&,J4.]P]-J9RPPRQ_@&E1E-Q7+F&I]OAV5=UH:/&S6Y?"
popcorn$=popcorn$+"c):*;Y7:)]61ZZU<((80a;/0Y)%Q;G?5[_K>]PaND.0<+<>@>VM`S@MVZH*NEAIG"
popcorn$=popcorn$+"R9TU,>U3V-@H@R4=XPUM_:G-E+S674;R)(YN_:&05G8Q`7@@?NO%J,bI8'9N_Nc:"
popcorn$=popcorn$+"88)+20a)52M;T_BOA45UR6[PcY^DTG;_@S-$09-,D4[<^Za2(UK`*\I[K.>0OPJE"
popcorn$=popcorn$+"E=]6a/$\W9EKWR4H*`S^@IHS2F(&J86*2M/2^a&7RGb9'>6OJ%&cPO)<M<[\1a)*"
popcorn$=popcorn$+"MFE]P'7-5NM9.?K+P('(3O,'1M=c1T4?F>T;7O5M`%BP4AX11a%F+_KQT-'\18`U"
popcorn$=popcorn$+"OJG:cN;<PbAa)FGHC@304bV1G3,+/BJD/>b`X6L:7X3U@SQWR;W::$K&:.8[CXH3"
popcorn$=popcorn$+"&\)V1IA,.7$0B:0&bYWR+GVDY2J.RT5Z\&&<^2+>F&8%Z1YC@<H>O8\HQ[P=&UBC"
popcorn$=popcorn$+"76JTCYF0@^)57YJ1(1T8G,-17=_3bIY;T/:LIb3=?616_KB7^;,16L.2>4'[8UXD"
popcorn$=popcorn$+"LaLSL7Y&-Q0E`JQ[>L3YX6`EP2T9$&^M+>K,>b11aG/ac0c9T,?YQY-4;L+3$S.c"
popcorn$=popcorn$+"`A<+TH2cc%?;[Rb=IRGb3O0/M$*1KFX^05MQTc$[O4YU8YL-T/NM>:V^7+)IT^XZ"
popcorn$=popcorn$+"['/R$?;Rb?KON:$]OE&(^_>ZbbX.:8CM\M&Cb4JH]=,T9X05aR1c(JcFQIQ2WCRI"
popcorn$=popcorn$+"aUIW_?c<^\[)'[b*YY=RM.DV%T$$"
popcorn_snd$=UNCOMPRESS$(INLINE$(popcorn$))

' c/xbasic/examples2/sound/liedsong.snd 5120 Bytes. (compressed: 593 Bytes, 11%)
liedsong$=""
liedsong$=liedsong$+";X$c717<;]%RbC`KaG*NT5S%_<KR\^,K_*1JJZ`O0V8/Q[&ccWS206H-K)X(R*MX"
liedsong$=liedsong$+"c)?<,XG(3@_D]N?*Y:[W$TEPU3@0BHJ\J`ED5FE'S4.1X/8/9JRGO[*1@3Va49Ua"
liedsong$=liedsong$+".PYDMHA.,/47NbDULL3UB3)S(>9Y6Z$B(<.Y/:b]7;ac`U*G8+Z6-3XLHM'UL>?F"
liedsong$=liedsong$+"KGVbQ:_P4?+CF+7&Y)C'\Zc$K(CIIc>TAK3@OCBBF:_G,],4,JKD$</'KDUK2E0J"
liedsong$=liedsong$+"X;KS`RJ5TL?)(Z7DCDD$`+cWO/8MY/>b00TE+aFM$[%0IZ'YRW7.&K$Z)IXF/&LW"
liedsong$=liedsong$+".$&EG][RCMA'b>X:`^AW*c,9^4T+7XbR''OZW;=U:*G2G,6c158Gb4O56+LNbV23"
liedsong$=liedsong$+"&SH@\TJKQ+H*Vb/EDYcB0F$+)4.3[]%J/HEM&F0(0(8@6%8G3RYAScS&;+',V<JX"
liedsong$=liedsong$+"U`?L_B,V+%W\AM%YF^S=Z_=^,WJ4LE1T=T&,_Q7>GVK31?WH*KI+(\XWOC\5?G$W"
liedsong$=liedsong$+"AS(,?>PJO`<GcG3Z7N:[4R[&1:Y:C7Wb3R(QIK:3IEc%2SGL5F)*'^YP*5Ra^0aA"
liedsong$=liedsong$+"J<1'aQ(L>E-Eb3]/PR1H7XB;JL;70HED(WY5.JZ2UQ[91Y[IbX/?<2J>2J3BDZ++"
liedsong$=liedsong$+"'4I@INX;I-c(;A&=*;b*+c*NG2QDIOQXZ.QSAaJ\M]O>PD>bH&1JI^M80M^&+]N5"
liedsong$=liedsong$+"[*$N(,/UONZH3/)\(a)@YK-=cBD,`(?Xa+T,V9+W=a:X$S_E_65QMY4O@ac/WB0W"
liedsong$=liedsong$+"_+8)'aaC'ZW%BN:@ZW=DS$4$"
liedsong_snd$=UNCOMPRESS$(INLINE$(liedsong$))

' Set the three channels to sqare wave (like it was on the ATARI)

WAVE 1,2
WAVE 2,2
WAVE 3,2

bin=1/40/max(speed,4)
name$="/sdcard/bas/liedsong.snd"
IF exist(name$) OR 1
  '  open "I",#1,name$
  '  b$=input$(#1,lof(#1))
  '  close #1
  b$=liedsong_snd$
  kammertona1=440
  PRINT name$
  FOR i=0 TO LEN(b$) STEP 2
    b=PEEK(VARPTR(b$)+i) and 255
    ' print i,b
    IF b=0
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt1=(pt1 AND -256) or b2
      IF pt1>0
        SOUND 1,140000/pt1
        '  pause bin
        '  sub b2,bin
      ELSE
        SOUND 1,0
      ENDIF
    ELSE if b=1
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt1=(pt1 AND (-256*256+255)) or b2*256
      IF pt1>0
        SOUND 1,140000/pt1
        '  pause bin
        '  sub b2,bin
      ELSE
        SOUND 1,0
      ENDIF

    ELSE if b=2
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt2=(pt2 AND -256) or b2
      IF pt2>0
        SOUND 2,140000/pt2
        '  pause bin
        '  sub b2,bin
      ELSE
        SOUND 2,0
      ENDIF

    ELSE if b=3
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt2=(pt2 AND (-256*256+255)) or b2*256
      IF pt2>0
        SOUND 2,140000/pt2
        '  pause bin
        '  sub b2,bin
      ELSE
        SOUND 2,0
      ENDIF

    ELSE if b=4
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt3=(pt3 AND -256) or b2
      IF pt3>0
        SOUND 3,140000/pt3
      ELSE
        SOUND 3,0
      ENDIF
    ELSE if b=5
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt3=(pt3 AND (-256*256+255)) or b2*256
      IF pt3>0
        SOUND 3,140000/pt3
      ELSE
        SOUND 3,0
      ENDIF
    ELSE if b=7
      b2=PEEK(VARPTR(b$)+i+1) and 255
      PRINT "TONKAN�LE:"'BIN$(b2,8)''
      se1=BTST(b2,0)
      se2=BTST(b2,1)
      se3=BTST(b2,2)
    ELSE if b=8
      lt1=PEEK(VARPTR(b$)+i+1) and 255
      SOUND 1,,lt1/15
    ELSE if b=9
      lt2=PEEK(VARPTR(b$)+i+1) and 255
      SOUND 2,,lt2/15
    ELSE if b=10
      lt3=PEEK(VARPTR(b$)+i+1) and 255
      SOUND 3,,lt3/15
    ELSE if b=11 OR b=12 OR b=13 OR b>=8*16
      b2=PEEK(VARPTR(b$)+i+1) and 255
      PRINT str$(i,5,5);": ";STR$(pt1,5,5);" ";STR$(pt2,5,5);" ";,pt3,b2,lt1,lt2,lt3
      PAUSE b2/50
    ELSE
      b2=PEEK(VARPTR(b$)+i+1) and 255
    ENDIF
  NEXT i
ELSE
  PRINT name$+" does not exist."
ENDIF
PRINT "the end."
END
