' Factorize (big) Integer numers into prime factors.
' with X11-Basic  >= V.1.23
'
DIM smallprimes&(1000000)
CLR anzprimes
smallprimes&(0)=2
INC anzprimes

INPUT "Enter a (big) number: ",a&
lim&=SQRT(a&)   ! Limit up to which the primes are searchd
PRINT "Calculating primes up to ";lim&;". Please wait..."
FOR i=1 TO DIM?(smallprimes&())-1
  b&=NEXTPRIME(smallprimes&(i-1))
  EXIT IF b&>lim&
  smallprimes&(i)=b&
NEXT i
anzprimes=i
PRINT "calculated ";anzprimes;" primes up to: ";b&

PRINT "Factorization:"
PRINT a&;"=";
FOR i=0 TO anzprimes-1
  WHILE (a& MOD smallprimes&(i))=0
    PRINT smallprimes&(i);"*";
    FLUSH
    a&=(a& DIV smallprimes&(i))
    lim&=SQRT(a&)
  WEND
  EXIT IF smallprimes&(i)>lim&
NEXT i
IF nextprime(a&-1)=a& OR a&=1
  PRINT a&
ELSE
  ' The number is too big and we cannot be sure that this is a prime
  PRINT "----incomplete test -----";a&
ENDIF
END
