<?xml version="1.0" encoding="utf-8"?>
<!-- This file is part of X11BASIC, the basic interpreter for Unix/X
 * ============================================================
 * X11BASIC is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 -->
<resources>

    <string name="app_name">X11-Basic</string>
    <string name="app_versionname">X11-Basic V.1.27</string>
    <string name="impressum">
<![CDATA[ <h5>Impressum</h5>
    X11-Basic für Android ist ein Produkt von:<br/>
    Markus Hoffmann<br/>
    E-Mail: kollo@users.sourceforge.net<br/>
    Copyright &#169; 2009-2019 Markus Hoffmann
    <p/>
    X11-Basic for Android  comes with ABSOLUTELY NO WARRANTY;<br/>
    This is free software, and you are welcome to redistribute it
    under the conditions of the GNU GENERAL PUBLIC LICENSE, Version 2.
    ]]>
    </string>
    <string name="copyr">&#169; 1997&#8211;2019 Markus Hoffmann</string>
    <string name="readme">
<![CDATA[Dies ist die Android-Portierung von X11-Basic, der BASIC Programmiersprache für UNIX. 
    <p>
    X11-Basic ist ein BASIC-Dialekt mit Grafik und speziellen Kommandos, um die Sonderfunktionen der
    entsprechenden Hardware-Platform und des Betriebssystems auszunutzen.
    <p>
    Die Syntax ähnelt stark der von GFA-Basic in der ursprünglichen Implementierung für den 
    ATARI-ST aus den 1980er und 1990er Jahren.
    <p> 
    Sie können die X11-Basic App als einfachen Taschenrechner für mathematische Formeln verwenden, 
    oder aber eben auch für ausgefeiltere Algorithmen und Programme, welche auch Grafik verwenden 
    können.<p>
    Es ist kein Quelltext-Editor in dieser App enthalten. Einen solchen Editor können Sie aber 
    leicht als separate App installieren, und dieser kann dann von X11-Basic aus benutzt werden. 
    X11-Basic Programme werden mit Hilfe eines solchen externen Editors erstellt und dann von 
    X11-Basic geladen und ausgeführt.
    <p>
    Die BASIC Programme müssen die Endung .bas haben und sollten in den Ordner 
    /storage/bas (bzw. /sdcard/bas) abgelegt werden. 
    <p> 
    Von dort aus können sie in X11-Basic mit Menu-->load und Menu-->run geladen und gestartet werden.
    <p>
    BASIC Programme können auch kompiliert werden. Hierbei wird der Quelltext in eine nur noch von 
    der Maschine lesbare Form gebracht (sogenannter Bytecode). Der Vorteil hierbei ist, daß die so 
    übersetzten und vorbereiteten Programme bis zu 20 mal schneller ausgeführt werden können.
    Das Kompilieren geschieht nach dem Laden eines BASIC Programms mit Menu-->kompilieren. 
    Hierbei werden Bytecodedateien mit der Endung .b erzeugt, welche Sie dann ganz normal 
    mit Menu-_>load, und Menu-->run laden und starten können.
    <p> 
    Kompilierte X11-Basic Programme sind sehr schnell (kein schelleres BASIC ist bisher 
    für Android bekannt). 
    <p> 
    Auf der Projekt-Homepage finden Sie mehr Details:
    <a href="http://x11-basic.sourceforge.net/">http://x11-basic.sourceforge.net/</a>
    (Hier gibt es auch ein komplettes Benutzerhandbuch, leider nur in englischer Sprache.)
    <p>
    Benutzen Sie bitte auch die Diskussionsseiten und das Forum bei Sourceforge für Ihre Rückmeldungen.
    <p>Die jeweils neueste Version dieser App finden sie als .apk Datei zum Runterladen auf der X11-Basic Projektseite
       bei Sourceforge. 
    <p>Eine .zip Datei mit einer großen Menge von weiteren Beispielprogrammen 
    können Sie von dieser Seite herunterladen: 
    <a href="http://sourceforge.net/projects/x11-basic/files/X11-Basic-programs/">http://sourceforge.net/projects/x11-basic/files/X11-Basic-programs/</a> 
    <p>
X11-BASIC ist freie Software und ist absolut OHNE GEWÄHR -- bitte lesen Sie die Datei
<a href="http://x11-basic.sourceforge.net/COPYING">http://x11-basic.sourceforge.net/COPYING</a> 
für Einzelheiten. <br>
(Im Wesendlichen heißt es darin, frei, offener Quelltext, benutzen und modifizieren 
Sie ihn wie Sie wünschen, bauen Sie ihn nicht in andere, nicht freie Software ein, keine 
irgendwiegeartete Gewähr oder Haftung, geben Sie mir nicht die Schuld, wenn es nicht funktioniert.)
    <p>
]]></string>
    <string name="news">
<![CDATA[<h4>Aktuelles</h4>
    Bitte Geduld! Es gibt vermutlich noch Fehler in der Android-Version von X11-Basic, 
    welche in den Versionen für Linux oder Windows nicht vorkommen und noch nicht verbessert wurden.
    Z.B. sind die Grafik-Funktionen, insbesondere die Eigabemethoden nicht alle fehlerfrei
    implementiert.
<p>
    PRINT und INPUT auf den Standard-Eingabe-/Ausgabekanälen werden von der X11-Basic App zu einer 
    eingebauten VT100-Terminal-Emulation geleitet.
    <p> Spezielle Android Hardware kann mit den Kommandos GPS, SPEAK and SENSOR() angesprochen werden. 
    <p>Sie können mir helfen, diese App noch weiter zu verbessern:
    Schildern Sie möglichst ausführlich den Fall, wenn Sie einen Fehlerbericht verfasssen.
    Nutzen Sie auch das X11-Basic Forum und die Diskussionsseiten dort. 
    Das Projekt ist Open Source, es sind also auch direkte Verbesserungen 
    am X11-Basic Quelltext willkommen.  
    <H4>Danksagungen</h4>
    Danke an alle, welche mir geholfen haben, dieses Programmpaket zu realisieren.
<p>
Vielen Dank an die Entwickler von GFA-Basic. Dieses BASIC brachte mich in den 1980er Jahren dazu, mit dem Programmieren 
anzufangen.
Viele Ideen und die Syntax der meisten Kommandos wurden von der  
ATARI ST Implementierung übernommen.
<p>
Danke an sourceforge.net dafür, daß sie diesem Projekt eine WEB-Präsenz verleihen.
<p>
Besonderer Dank gilt folgenden Leuten, welche mir erst kürzlich geholfen haben:<br>
2013:<br>
* Matthias Vogl (va_copy patch for 64bit version)<br>
* Eckhard Kruse (for permission to include ballerburg.bas in the samples)<br>
* Marcos Cruz (beta testing and bug fixing)<br>
* James V. Fields (beta testing and correcting the manual)<br>
2014:<br>
* John Clemens, Sławomir Donocik, Izidor Cicnapuz, Christian Amler, <br> 
  Marcos Cruz, Charles Rayer, Bill Hoyt, and John Sheales (beta testing and bug fixing)<br>
2015:<br>
* Guillaume Tello, Wanderer, and John Sheales  (beta testing and bug fixing)<br>
2016:<br>
* John Sheales  (beta testing and bug fixing)<br>
* bruno xxx (helping with the compilation tutorial for Android)<br>
2017:<br>
* John Sheales  (beta testing and bug fixing)<br>
* David Hall  (bug fixing)<br>
* Emil Schweikerdt  (bug fixing)<br>
in 2018:<br>
* Alan (beta testing, bugfix)<br>
* John Sheales  (beta testing and bug fixing)<br>
in 2019:<br>
* Yet Another Troll (beta testing and bug fixing)<br>


<p>
X11-Basic baut auf einer Vielzahl von Software auf, welche von vielen Leuten beigesteuert wurde. 
Erwähnt seien hier nur die Libraries, welche unmittelbar mit dem X11-Basic Paket mitkommen, nämlich:
<p>
 the GNU multiple precision arithmetic library, Version 5.1.3.
   Copyright 1991, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
2001, 2002, 2003, 2004, 2005, 2006, 2007 Free Software Foundation, Inc.

For details, see: https://gmplib.org/
 <p>
LAPACK (Linear Algebra Package) is a standard software library for 
numerical linear algebra. 

For details, see: http://www.netlib.org/lapack/
 <p>
X11-Basic uses the lodepng code for the PNG bitmap graphics support.
Copyright &#169; 2005-2013 Lode Vandevenne
<p>
 X11-Basic uses a modified version of the libusb library for accessing the USB-Port
<p>

<h4>Letzte Änderungen in Version 1.26 (June 2018 -- June 2019)</h4>
- New functions/commands GPIO (Raspberry)<br>
- fixed bug in virtual-machine/compiler (RasPi, signed char)<br>
- new shortcut compiler start ybasic<br>
- fbxbasic included <br>
- added broadcast permission for UDP sockets<br>
- new syntax highlighting file for nano/pico<br>
- new commandline function --quitonend<br>
- bugfix in FOR (compiler) and better error handling<br>

<h4>Letzte Änderungen in Version 1.27 (June 2019 -- )</h4>
- removed VERSION and VERSION_DATE from library<br>
- new function REGEXP() and MATCH()<br>
- little fix for HELP, REM and DATA<br>

    ]]>
    </string>
    <string name="menu_about">Über</string>
    <string name="menu_quit">Beenden</string>
    <string name="menu_run">Run</string>
    <string name="menu_load">Laden &#8230;</string>
    <string name="menu_stop">Stop/Cont</string>
    <string name="menu_new">New</string>
    <string name="menu_compile">Kompilieren</string>
    <string name="menu_editor">Editor</string>
    <string name="menu_help">Hilfe&#8230;</string>
    <string name="menu_settings">Info/Einstellungen&#8230;</string>
    <string name="word_settings">Einstellungen</string>
    
    <string name="menu_fontsize">Schriftgröße auswählen</string>
    <string name="menu_focus">Bildschirm-Fokus auswählen</string>
    <string name="menu_splashscreen">Splash screen</string>
    <string name="menu_splashscreen2">Zeige Version-Info beim X11-Basic Start.</string>
    <string name="menu_keyboard">Tastatur einblenden</string>
    <string name="menu_paste">Einfügen vom Clipboard</string>
    
    <string name="menu_dispkey">Tastatur</string>
    <string name="menu_dispkey2">Tastatur einblenden bei Programmstart</string>
    <string name="menu_dispheader">Titel</string>
    <string name="menu_dispheader2">Titelzeile einblenden</string>
    <string name="menu_dispstatus">Status</string>
    <string name="menu_dispstatus2">Statuszeile einblenden</string>
    <string name="word_version">Version</string>
    <string name="word_copyright">Copyright</string>
    <string name="word_search">Suchen:</string>
    <string name="word_ok">OK</string>
    <string name="word_keyword">BASIC Stichwort</string>
  
     <string name="word_searchcomment">Stichwort oder Kommando eingeben.</string>
    
    <string name="about_market_app">Diese App im Android Market</string>
    <string name="about_homepage">Project Homepage</string>
    <string name="about_guestbook">Gästebuch</string>
    <string name="about_wiki">X11-Basic Forum</string>
    <string name="about_manual">Bedienungsanleitung (englisch)</string>
    <string name="about_market_publisher">Mehr vom Autor dieser App</string>
    <string name="about_example_programs">X11-Basic Beispielprogramme
        (.bas Quelltexte) zum Runterladen</string>
    <string name="preferences_credits_ttconsole_title">Ein Dankeschön: Die TomTom Console</string>
    <string name="edit_text">Edit text</string>
    <string name="select_text">Select text</string>
    <string name="copy_all">Copy all</string>
    <string name="paste">Einfügen</string>
    <string name="editor_notfound">
<![CDATA[<h4>Editor nicht gefunden!</h4> 
       Es ist kein Text-Editor installiert, welcher benutzt werden kann.
       Sie können aber einen installieren. Sehr empfehlenswert ist z.B. die App
       "920 Text Editor". Der geht gut.
    ]]>
    </string>
    <string name="editor_notb">
<![CDATA[<h2>Error!</h2> 
       Sie können kompilierten code (binary) nicht editieren! Bitte laden Sie den zugehörigen
       .bas Quellcode, editieren Sie den, und kompilieren Sie dann neu. 
    ]]>
    </string>   
    <string name="sdcard_notb">
<![CDATA[<h2>ACHTUNG!</h2> 
       Ihr Smartphone oder Tablet stellt offenbar keine brauchbare Speichermöglichkeit für X11-Basic Programme zur Verfügung.
       Ein externer Speicher (SD-Karte) ist nicht nutzbar.
       X11-Basic wird stattdessen versuchen, den internen Speicher zu verwenden. 
       Dies hat aber den Nachteil, daß Sie evtl. nicht in der Lage sein werden, X11-Basic Programme zu editieren oder 
       neu zu erstellen.  Außerdem werden alle Änderungen oder neu erstellte Dateien hier zusammen mit der X11-Basic App
       entfernt (und sind dann unwiederbringlich verloren), wenn sie diese deinstallieren. Bitte überprüfen Sie unbedingt 
       vorher, ob Dateien in {0} von anderen Apps aus gelesen werden können, und wenn so, 
       machen Sie immer Sicherheitskopien ihrer Arbeit auf einem externen Medium.
    ]]>
    </string>  
    <string name="word_cancel">Abbruch</string>
    <string name="word_proceed">Weiter</string>
    
    <!-- Crash report dialog -->
    <string name="crash_subject">Crash: X11-Basic v{0} on {1} / {2} / {3}</string>
    <string name="crash_preamble">Bitte beschreiben Sie kurz, 
        was Sie kurz vor dem Absturz gemacht haben, wenn möglich, dann drücken Sie bitte auf
        Absenden:</string>
    <string name="crashtitle">Mann, das ist ja blöd&#8230;</string>
    <string name="crashed">Entschuldigung, irgendwas hat X11-Basic zum Abstürzen 
        gebracht. 
        Es musste geschlossen werden. Wenn Sie der Meinung sind, daß nicht Sie selbst den Absturz
        verursacht haben (z.B. durch unvorsichtiges Verwenden des POKE Befehls
        oder dergleichen), möchten Sie vielleicht einen 
        Absturzbericht an den Autor der X11-Basic App senden, der helfen könnte, 
        das Problem zu beseitigen.
        </string>
    <string name="report">Bericht</string>
    <string name="close">Schließen</string>
    <string name="author_email">dr.markus.hoffmann@gmx.de</string>
    <string name="email_subject">X11Basic v{0} on {1} / {2} / {3}</string>
    <string name="unknown_version">UNBEKANNT</string>
    
    <!-- Messages id X11BasicActivity -->
    
    <string name="word_interrupt">Interrupt.</string>
    <string name="word_done">fertig.</string>
    <string name="message_shortcut_created">Die Verknüpfung \'%s\' auf dem Desktop wurde erstellt.</string>
    <string name="message_nothing_to_do">Das Programm ist leer. Es gibt nichts zu tun.</string>
    <string name="message_nothing_to_do2">Es wurde kein Programm angegeben. Es gibt nichts zu tun.</string>
    <string name="message_compilation_success">Kompilieren erfolgreich. Speichere als %s.</string>
    <string name="error_backgroundtask_dead">ERROR: Background task seems to be dead.</string>
    <string name="error_compilation">ERROR: compilation failed.</string>
    <string name="error_editor">FEHLER: Der Editor konnte nicht gestartet werden. Meldung: %s</string>
    <string name="error_intend">FEHLER: Der Intend konnte nicht aufgerufen werden. Meldung: %s</string>
    <string name="message_really_delete">Wirklich löschen?</string>
    <string name="message_delete">Sie sind dabei, die Datei %s zu löschen. Wollen Sie wirklich fortfahren?</string>
    <string name="message_select_file_shortcut">Wählen Sie eine Datei zum Direktstart aus:</string>
    <string name="message_select_file_load">Zu ladende Datei:</string>
    <string name="message_reloaded">%s wurde neu geladen.</string>
</resources>
