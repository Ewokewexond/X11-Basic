package net.sourceforge.x11basic;

/* CrashHandlerjava (c) 2011-2015 by Markus Hoffmann
 *
 * This file is part of X11-Basic for Android, (c) by Markus Hoffmann 1991-2015
 * ============================================================================
 * X11-Basic for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 
 

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/* X11-Basic crash handler. The handler displays a message to the user and
   asks if he wants to send an email with crash information to the author of the 
   app. If the user confirms, it collects crash information and execute the email
   app, where all information collected is shown to the user. The user can add a
   comment and then he is asked to press send to actually send the mail.*/

public class CrashHandler extends Activity {
	public static final String TAG = "CrashHandler";
	String log;
	Process process;
	TextView cl;

	protected void onCreate(Bundle state) {
		super.onCreate(state);
		setTitle(R.string.crashtitle);
		setContentView(R.layout.crashhandler);
		final Button b = (Button) findViewById(R.id.report), c = (Button) findViewById(R.id.close);
		cl = (TextView) findViewById(R.id.log);

		try {
			process = Runtime.getRuntime().exec(new String[] {"logcat", "-d", "-v", "threadtime"});
			log = X11BasicActivity.readAllOf(process.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			Toast.makeText(CrashHandler.this, e.toString(), Toast.LENGTH_LONG).show();
		}
		cl.setText("log:" + log);
		/* OK Button: execute tryEmailAuthor */
		b.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				boolean ok = X11BasicActivity.tryEmailAuthor(CrashHandler.this,
						true, getString(R.string.crash_preamble)
						+ "\n\n\n\nLog:\n" + log);
				if(ok) finish();
			}
		});
		/* CANCEL Button: just finish */
		c.setOnClickListener(new View.OnClickListener() {public void onClick(View v) {finish();}});
	}
	@Override
	public void onStart() { /* On start collect log data...*/
		super.onStart();
		final android.content.Intent intent = getIntent();
		if (intent != null) {
			final Bundle b = intent.getExtras();
			// Log.d (TAG, "> Got : " + b.toString());
			log = "-> " + b.getString(Intent.EXTRA_TEXT) + "\n";
			cl.setText(log);
		}
	}
}
