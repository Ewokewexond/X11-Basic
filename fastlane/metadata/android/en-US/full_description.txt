X11-Basic is a dialect of the BASIC programming language with graphics
capability. It has a very rich command set, though it is still easy to learn.
The syntax is most similar to GFA-Basic for ATARI-ST. It is a structured dialect
with no line numbers. A full manual and command reference is available.
GFA-programs should run with only a few changes. Also DOS/QBASIC programmers
will feel comfortable. This implementation is one of the fastest basic
interpreters for Android. Programs can be compiled into a platform independant
bytecode.
