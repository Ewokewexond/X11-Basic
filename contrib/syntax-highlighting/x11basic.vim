" x11basic.vim
" Vim syntax file
" Language:   X11-Basic
" Maintainer: Marcos Cruz (programandala.net)
" URL:        http://programandala.net
" Updated:    2012-11-07

" Notes:

" This is a work in progress.
" The unfinished issues are marked with 'xxx'.
" The last version of this file can be found at
" <http://programandala.net/en.program.x11-basic_vim_syntax_file>.

" The history is at the end of this file.

" Usage:

" (In this text the '<' and '>' chars are used as separators for
" filenames and code).
"
" Copy or link this file to the following directory:
" <~/.vim/syntax/>.
"
" In case you use the 'bas' filename extension for X11-Basic
" programs, Vim will use its standard BASIC syntax file and you
" will have to change it manually with the following command:
" <:set filetype=x11basic>.
"
" You could change the default filetype associated with the
" 'bas' filename extension (read the Vim documentation, it's
" excellent), but it's more practical to use a different
" filename extension, e.g. 'xbas'.  Then all you need is to add
" (in one single line) the command <autocmd BufNewFile,BufRead
" *.xbas setfiletype x11basic> at the end of the file
" <~/.vim/filetype.vim>.

" To-do:

" Match different usages of keywords, e.g.:
" ECHO ON (command) vs ON * GOSUB (program flow).
" AND() function vs AND operator.

if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

if version > 600
  setlocal iskeyword=48-57,65-90,97-122,$,?,_,%
else
  set iskeyword=48-57,65-90,97-122,$,?,_,%
endif

syn case ignore
syn sync minlines=10 maxlines=500

syn keyword x11basicCommand &
syn keyword x11basicCommand ?
syn keyword x11basicCommand ADD
syn keyword x11basicCommand AFTER
syn keyword x11basicCommand ALERT
syn keyword x11basicCommand ARRAYCOPY
syn keyword x11basicCommand ARRAYFILL
syn keyword x11basicCommand BEEP
syn keyword x11basicCommand BELL
syn keyword x11basicCommand BGET
syn keyword x11basicCommand BLOAD
syn keyword x11basicCommand BMOVE
syn keyword x11basicCommand BOUNDARY
syn keyword x11basicCommand BOX
syn keyword x11basicCommand BPUT
syn keyword x11basicCommand BSAVE
syn keyword x11basicCommand CHAIN
syn keyword x11basicCommand CIRCLE
syn keyword x11basicCommand CLEAR
syn keyword x11basicCommand CLEARW
syn keyword x11basicCommand CLIP
syn keyword x11basicCommand CLOSE
syn keyword x11basicCommand CLOSEW
syn keyword x11basicCommand CLR
syn keyword x11basicCommand CLS
syn keyword x11basicCommand COLOR
syn keyword x11basicCommand CONNECT
syn keyword x11basicCommand COPYAREA
syn keyword x11basicCommand CURVE
syn keyword x11basicCommand DATA
syn keyword x11basicCommand DEC
syn keyword x11basicCommand DEFFILL
syn keyword x11basicCommand DEFLINE
syn keyword x11basicCommand DEFMARK
syn keyword x11basicCommand DEFMOUSE
syn keyword x11basicCommand DEFTEXT
syn keyword x11basicCommand DELAY
syn keyword x11basicCommand DIM
syn keyword x11basicCommand DIV
syn keyword x11basicCommand DPOKE
syn keyword x11basicCommand DRAW
syn keyword x11basicCommand DUMP
syn keyword x11basicCommand ECHO
syn keyword x11basicCommand EDIT
syn keyword x11basicCommand ELLIPSE
syn keyword x11basicCommand ERASE
syn keyword x11basicCommand ERROR
syn keyword x11basicCommand EVAL
syn keyword x11basicCommand EVENT
syn keyword x11basicCommand EVERY
syn keyword x11basicCommand EXEC
syn keyword x11basicCommand FFT
syn keyword x11basicCommand FILESELECT
syn keyword x11basicCommand FILL
syn keyword x11basicCommand FIT
syn keyword x11basicCommand FIT_LINEAR
syn keyword x11basicCommand FLUSH
syn keyword x11basicCommand FORM_INPUT
syn keyword x11basicCommand FREE
syn keyword x11basicCommand FULLW
syn keyword x11basicCommand GET
syn keyword x11basicCommand GET_GEOMETRY
syn keyword x11basicCommand GET_SCREENSIZE
syn keyword x11basicCommand GPRINT
syn keyword x11basicCommand GPS
syn keyword x11basicCommand GRAPHMODE
syn keyword x11basicCommand HELP
syn keyword x11basicCommand HIDEM
syn keyword x11basicCommand HOME
syn keyword x11basicCommand HTAB
syn keyword x11basicCommand INC
syn keyword x11basicCommand INFOW
syn keyword x11basicCommand INPUT
syn keyword x11basicCommand KEYEVENT
syn keyword x11basicCommand LET
syn keyword x11basicCommand LINE
syn keyword x11basicCommand LINEINPUT
syn keyword x11basicCommand LINK
syn keyword x11basicCommand LIST
syn keyword x11basicCommand LOAD
syn keyword x11basicCommand LOCAL
syn keyword x11basicCommand LOCATE
syn keyword x11basicCommand LPOKE
syn keyword x11basicCommand LSET
syn keyword x11basicCommand LTEXT
syn keyword x11basicCommand MENU
syn keyword x11basicCommand MENUDEF
syn keyword x11basicCommand MENUKILL
syn keyword x11basicCommand MENUSET
syn keyword x11basicCommand MERGE
syn keyword x11basicCommand MOTIONEVENT
syn keyword x11basicCommand MOUSE
syn keyword x11basicCommand MOUSEEVENT
syn keyword x11basicCommand MOVEW
syn keyword x11basicCommand MUL
syn keyword x11basicCommand NEW
syn keyword x11basicCommand NOOP
syn keyword x11basicCommand NOP
syn keyword x11basicCommand OBJC_ADD
syn keyword x11basicCommand OBJC_DELETE
syn keyword x11basicProgramFlow ON
syn keyword x11basicCommand ONMENU
syn keyword x11basicCommand OPEN
syn keyword x11basicCommand OPENW
syn keyword x11basicCommand OUT
syn keyword x11basicCommand PAUSE
syn keyword x11basicCommand PBOX
syn keyword x11basicCommand PCIRCLE
syn keyword x11basicCommand PELLIPSE
syn keyword x11basicCommand PIPE
syn keyword x11basicCommand PLAYSOUND
syn keyword x11basicCommand PLIST
syn keyword x11basicCommand PLOT
syn keyword x11basicCommand POKE
syn keyword x11basicCommand POLYFILL
syn keyword x11basicCommand POLYLINE
syn keyword x11basicCommand POLYMARK
syn keyword x11basicCommand PRBOX
syn keyword x11basicCommand PRINT
syn keyword x11basicCommand PROCEDURE
syn keyword x11basicCommand PSAVE
syn keyword x11basicCommand PUT
syn keyword x11basicCommand PUTBACK
syn keyword x11basicCommand PUT_BITMAP
syn keyword x11basicCommand RANDOMIZE
syn keyword x11basicCommand RBOX
syn keyword x11basicCommand READ
syn keyword x11basicCommand RELSEEK
syn keyword x11basicCommand RESTORE
syn keyword x11basicCommand RSRC_FREE
syn keyword x11basicCommand RSRC_LOAD
syn keyword x11basicCommand SAVE
syn keyword x11basicCommand SAVESCREEN
syn keyword x11basicCommand SAVEWINDOW
syn keyword x11basicCommand SCOPE
syn keyword x11basicCommand SCREEN
syn keyword x11basicCommand SEEK
syn keyword x11basicCommand SEND
syn keyword x11basicCommand SENSOR
syn keyword x11basicCommand SETFONT
syn keyword x11basicCommand SETENV
syn keyword x11basicCommand SETMOUSE
syn keyword x11basicCommand SGET
syn keyword x11basicCommand SHOWPAGE
syn keyword x11basicCommand SHM_DETACH
syn keyword x11basicCommand SHM_FREE
syn keyword x11basicCommand SIZEW
syn keyword x11basicCommand SORT
syn keyword x11basicCommand SOUND
syn keyword x11basicCommand SPEAK
syn keyword x11basicCommand SPLIT
syn keyword x11basicCommand SPUT
syn keyword x11basicCommand SUB
syn keyword x11basicCommand SWAP
syn keyword x11basicCommand SYSTEM
syn keyword x11basicCommand TEXT
syn keyword x11basicCommand TITLEW
syn keyword x11basicCommand TOPW
syn keyword x11basicCommand BOTTOMW
syn keyword x11basicCommand TROFF
syn keyword x11basicCommand TRON
syn keyword x11basicCommand UNLINK
syn keyword x11basicCommand USING
syn keyword x11basicCommand VERSION
syn keyword x11basicCommand VOID
syn keyword x11basicCommand VSYNC
syn keyword x11basicCommand WATCH
syn keyword x11basicCommand WAVE
syn keyword x11basicCommand WORT_SEP
syn keyword x11basicCommand XLOAD
syn keyword x11basicCommand XRUN
syn keyword x11basicFunction ABS
syn keyword x11basicFunction ACOS
syn keyword x11basicFunction ACOSH
syn keyword x11basicFunction ACOT
syn keyword x11basicFunction ACOTH
syn keyword x11basicFunction ACSC
syn keyword x11basicFunction ACSCH
syn keyword x11basicFunction ADD
syn keyword x11basicFunction AND
syn keyword x11basicFunction ARRPTR
syn keyword x11basicFunction ASC
syn keyword x11basicFunction ASEC
syn keyword x11basicFunction ASECH
syn keyword x11basicFunction ASIN
syn keyword x11basicFunction ASINH
syn keyword x11basicFunction ATAN
syn keyword x11basicFunction ATAN2
syn keyword x11basicFunction ATANH
syn keyword x11basicFunction ATN
syn keyword x11basicFunction BCHG
syn keyword x11basicFunction BCLR
syn keyword x11basicFunction BIN$
syn keyword x11basicFunction BSET
syn keyword x11basicFunction BTST
syn keyword x11basicFunction BWTD$
syn keyword x11basicFunction BWTE$
syn keyword x11basicFunction BYTE
syn keyword x11basicFunction CARD
syn keyword x11basicFunction CBRT
syn keyword x11basicFunction CEIL
syn keyword x11basicFunction CHR$
syn keyword x11basicFunction CINT
syn keyword x11basicFunction COLS
syn keyword x11basicFunction COMBIN
syn keyword x11basicFunction COMPRESS$
syn keyword x11basicFunction COS
syn keyword x11basicFunction COSH
syn keyword x11basicFunction COT
syn keyword x11basicFunction COTH
syn keyword x11basicFunction CRC
syn keyword x11basicFunction CRSCOL
syn keyword x11basicFunction CRSLIN
syn keyword x11basicFunction CSC
syn keyword x11basicFunction CSCH
syn keyword x11basicFunction CTIMER
syn keyword x11basicFunction CVA
syn keyword x11basicFunction CVD
syn keyword x11basicFunction CVF
syn keyword x11basicFunction CVI
syn keyword x11basicFunction CVL
syn keyword x11basicFunction CVS
syn keyword x11basicFunction DATE$
syn keyword x11basicFunction DEG
syn keyword x11basicFunction DET
syn keyword x11basicFunction DIM?
syn keyword x11basicFunction DIV
syn keyword x11basicFunction DPEEK
syn keyword x11basicFunction ENV$
syn keyword x11basicFunction EOF
syn keyword x11basicFunction EQV
syn keyword x11basicFunction ERR
syn keyword x11basicFunction ERR$
syn keyword x11basicFunction EVAL
syn keyword x11basicFunction EVEN
syn keyword x11basicFunction EXEC
syn keyword x11basicFunction EXIST
syn keyword x11basicFunction EXP
syn keyword x11basicFunction EXPM1
syn keyword x11basicFunction FACT
syn keyword x11basicFunction FAK
syn keyword x11basicFunction FALSE
syn keyword x11basicFunction FILEEVENT$
syn keyword x11basicFunction FIX
syn keyword x11basicFunction FLOOR
syn keyword x11basicFunction FORK
syn keyword x11basicFunction FORM_ALERT
syn keyword x11basicFunction FORM_CENTER
syn keyword x11basicFunction FORM_DIAL
syn keyword x11basicFunction FORM_DO
syn keyword x11basicFunction FRAC
syn keyword x11basicFunction FREEFILE
syn keyword x11basicFunction GASDEV
syn keyword x11basicFunction GET_COLOR
syn keyword x11basicFunction GLOB
syn keyword x11basicFunction GRAY
syn keyword x11basicFunction HEX$
syn keyword x11basicFunction HYPOT
syn keyword x11basicFunction IMP
syn keyword x11basicFunction INKEY$
syn keyword x11basicFunction INLINE$
syn keyword x11basicFunction INP
syn keyword x11basicFunction INP%
syn keyword x11basicFunction INP&
syn keyword x11basicFunction INP?
syn keyword x11basicFunction INPUT$
syn keyword x11basicFunction INSTR
syn keyword x11basicFunction INT
syn keyword x11basicFunction INV
syn keyword x11basicFunction IOCTL
syn keyword x11basicFunction JULDATE$
syn keyword x11basicFunction JULIAN
syn keyword x11basicFunction LCASE$
syn keyword x11basicFunction LEFT$
syn keyword x11basicFunction LEN
syn keyword x11basicFunction LN
syn keyword x11basicFunction LN
syn keyword x11basicFunction LOC
syn keyword x11basicFunction LOF
syn keyword x11basicFunction LOG
syn keyword x11basicFunction LOG10
syn keyword x11basicFunction LOG1P
syn keyword x11basicFunction LOGB
syn keyword x11basicFunction LOWER$
syn keyword x11basicFunction LPEEK
syn keyword x11basicFunction MALLOC
syn keyword x11basicFunction MAX
syn keyword x11basicFunction MID$
syn keyword x11basicFunction MIN
syn keyword x11basicFunction MKA$
syn keyword x11basicFunction MKD$
syn keyword x11basicFunction MKF$
syn keyword x11basicFunction MKI$
syn keyword x11basicFunction MKL$
syn keyword x11basicFunction MKS$
syn keyword x11basicFunction MOD
syn keyword x11basicFunction MOUSEK
syn keyword x11basicFunction MOUSEX
syn keyword x11basicFunction MOUSEY
syn keyword x11basicFunction MTFD$
syn keyword x11basicFunction MTFE$
syn keyword x11basicFunction MUL
syn keyword x11basicFunction OBJC_DRAW
syn keyword x11basicFunction OBJC_FIND
syn keyword x11basicFunction OBJC_OFFSET
syn keyword x11basicFunction OCT$
syn keyword x11basicFunction ODD
syn keyword x11basicFunction PC
syn keyword x11basicFunction PEEK
syn keyword x11basicFunction PI
syn keyword x11basicFunction POINT
syn keyword x11basicFunction PRG$
syn keyword x11basicFunction PTST
syn keyword x11basicFunction RAD
syn keyword x11basicFunction RAND
syn keyword x11basicFunction RANDOM
syn keyword x11basicFunction REPLACE$
syn keyword x11basicFunction REVERSE$
syn keyword x11basicFunction RIGHT$
syn keyword x11basicFunction RINSTR
syn keyword x11basicFunction RLD$
syn keyword x11basicFunction RLE$
syn keyword x11basicFunction RND
syn keyword x11basicFunction ROUND
syn keyword x11basicFunction ROWS
syn keyword x11basicFunction RSRC_GADDR
syn keyword x11basicFunction SEC
syn keyword x11basicFunction SECH
syn keyword x11basicFunction SENSOR
syn keyword x11basicFunction SGN
syn keyword x11basicFunction SHM_ATTACH
syn keyword x11basicFunction SHM_MALLOC
syn keyword x11basicFunction SIN
syn keyword x11basicFunction SINH
syn keyword x11basicFunction SOLVE
syn keyword x11basicFunction SP
syn keyword x11basicFunction SPACE$
syn keyword x11basicFunction SQR
syn keyword x11basicFunction SQRT
syn keyword x11basicFunction SRAND
syn keyword x11basicFunction STIMER
syn keyword x11basicFunction STR$
syn keyword x11basicFunction STRING$
syn keyword x11basicFunction SWAP
syn keyword x11basicFunction SYM_ADR
syn keyword x11basicFunction SYSTEM$
syn keyword x11basicFunction TAN
syn keyword x11basicFunction TANH
syn keyword x11basicFunction TERMINALNAME$
syn keyword x11basicFunction TERMINALNAME$
syn keyword x11basicFunction TIME$
syn keyword x11basicFunction TIMER
syn keyword x11basicFunction TRIM$
syn keyword x11basicFunction TRUE
syn keyword x11basicFunction TRUNC
syn keyword x11basicFunction UCASE$
syn keyword x11basicFunction UNIXDATE$
syn keyword x11basicFunction UNIXTIME$
syn keyword x11basicFunction UPPER$
syn keyword x11basicFunction VAL
syn keyword x11basicFunction VAL?
syn keyword x11basicFunction VARPTR
syn keyword x11basicFunction WORD
syn keyword x11basicFunction WORD$
syn keyword x11basicFunction WORT_SEP
syn keyword x11basicFunction XOR
syn keyword x11basicFunction XTRIM$
syn keyword x11basicOperator AND
syn keyword x11basicOperator NAND
syn keyword x11basicOperator NOT
syn keyword x11basicOperator OR
syn keyword x11basicOperator XOR
syn keyword x11basicProgramFlow BREAK
syn keyword x11basicProgramFlow CALL
syn keyword x11basicProgramFlow CASE
syn keyword x11basicProgramFlow CONT
syn keyword x11basicProgramFlow DEFAULT
syn keyword x11basicProgramFlow DO
syn keyword x11basicProgramFlow DOWNTO
syn keyword x11basicProgramFlow ELSE
syn keyword x11basicProgramFlow END
syn keyword x11basicProgramFlow ENDFUNC
syn keyword x11basicProgramFlow ENDFUNCTION
syn keyword x11basicProgramFlow ENDIF
syn keyword x11basicProgramFlow ENDSELECT
syn keyword x11basicProgramFlow EXIT
syn keyword x11basicProgramFlow FOR
syn keyword x11basicProgramFlow FUNCTION
syn keyword x11basicProgramFlow GOSUB
syn keyword x11basicProgramFlow GOTO
syn keyword x11basicProgramFlow IF
syn keyword x11basicProgramFlow LOOP
syn keyword x11basicProgramFlow NEXT
syn keyword x11basicProgramFlow QUIT
syn keyword x11basicProgramFlow REPEAT
syn keyword x11basicProgramFlow RESUME
syn keyword x11basicProgramFlow RETURN
syn keyword x11basicProgramFlow RUN
syn keyword x11basicProgramFlow SELECT
syn keyword x11basicProgramFlow STEP
syn keyword x11basicProgramFlow TO
syn keyword x11basicProgramFlow UNTIL
syn keyword x11basicProgramFlow WEND
syn keyword x11basicProgramFlow WHILE

syn region x11basicString start='"' skip='""' end='"' 

" Comments

syn match x11basicTodo /TODO/ contained
syn match x11basicTodo /FIXME/ contained
syn match x11basicTodo /XXX/ contained

syn region x11basicComment start="^\s*rem\>" end="$" contains=x11basicTodo
syn region x11basicComment start="^\s*'" end="$" contains=x11basicTodo
syn region x11basicComment start="^\s*#" end="$" contains=x11basicTodo
syn region x11basicComment start="^\s*!" end="$" contains=x11basicTodo
" xxx this '!' is not fully tested:
syn region x11basicComment start="\s!\s" end="$" contains=x11basicTodo

" Math operators

" xxx unfinished:
syn match x11basicOperator "[\+\-\=\|\*\/\>\<\%\()[\]]" contains=x11basicParen

" The default methods for highlighting. Can be overridden later

"hi def link x11basicKeyword Statement
hi def link x11basicFunction Function
hi def link x11basicCommand Statement
hi def link x11basicProgramFlow Statement
hi def link x11basicComment Comment
hi def link x11basicTodo Todo
hi def link x11basicString String
hi def link x11basicOperator Statement
"hi def link x11basicIdentifier Statement

let b:current_syntax = "x11basic"

" vim: ts=2

" History:
"
" 2012-10-25 First version.
"
" 2012-10-26 Missing keywords: TRIM$(), SPLIT, AND(), ATN(), BCHG(), BCLR(),
" BSET(), BTST(), BWTD$(), BWTE$(), BYTE(), CARD(), CEIL(), COMBIN(),
" COMPRESS$(), CRC(), CVA(), DET(), DIV(), EQV(), ERR$(), EXPM1(), FACT(),
" FILEEVENT$(), FIX(), FLOOR(), FORK(), FORM_CENTER(), IMP(), INV(), IOCTL(),
" LN(), LOGB(), LOG1P(), MALLOC(), MKS$(), MKA$(), MOD(), MTFD$(), MTFE$(),
" MUL(), OBJECT_OFFSET(), PTST(), REPLACE$(), WHILE, WEND, REVERSE$(), RLD$(),
" RLE$(), ROUND(), SENSOR(), SHM_ATTACH(), SOLVE(), SRAND(), SWAP(), WORD(),
" WORD$(), WORT_SEP(), XOR(), AFTER, BOUNDARY, CLIP, CONNECT, COPYAREA, CURVE,
" DELAY, EVENT, FILL, FREE, GPS, HTAB, OBJC_ADD, OBJC_DELETE, PIPE, PLAYSOUND,
" SAVESCREEN, SAVEWINDOW, SCREEN, SEND, SENSOR, SETFONT, SETMOUSE, SHM_DETACH,
" SHM_FREE, SIZEW, TOPW, WATCH, WAVE, GET_SCREENSIZE, GET_GEOMETRY, BOTTOMW.
"
" 2012-10-27 Missing keywords: USING, VAR, ABS(). 

" 2012-10-28 Missing keywords: CRSCOL, CRSLIN.

" 2012-10-29 Missing undocumented keyword: XTRIM$(). Removed the keywords of
" the SELECT structure (it's in the manual but not implemented yet).

" 2012-10-31 Added the SELECT structure again; it's been
" implemented in version 1.19-6 of the language. Removed VAR,
" it's in the manual but not implemented yet. Updated the '!'
" comment mark: in version 1.19-6 it can be used also at the
" start of the line.

" 2012-11-07 Usage instructions. '!' comment updated.
