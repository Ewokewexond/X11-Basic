' by Markus Hoffmann 2014
' This program builds a .apk file out of an X11-Basic program (.bas or .b)
' ready to be installed on an Android device.
'
' (not working yet)
' The whole thing should in the end run on the Android device itself, but
' I got really stuck here.
'
x11basicapk$="X11-Basic.apk"
tmpdir$="/tmp/apkbuild/"
SYSTEM "rm -rf "+tmpdir$
MKDIR tmpdir$
basfile$="test.bas"
outputfilename$="b.apk"
IF NOT EXIST(x11basicapk$)
  ~FORM_ALERT(1,"[3][xbc: ERROR: X11-Basic.apk not found.][CANCEL]")
  PRINT "xbc: ERROR: X11-Basic.apk not found."
  QUIT
ENDIF
SYSTEM "cd "+tmpdir$+" ; apktool d  "+env$("PWD")+"/"+x11basicapk$

' system "cd "+tmpdir$+"X11-Basic/ ; rm -rf assets/manual"
SYSTEM "cd "+tmpdir$+"X11-Basic/assets/bas ; rm -rf sound"
SYSTEM "cd "+tmpdir$+"X11-Basic/assets/bas ; rm -f [bcdefghijklmnopqrstuvwexz]*.bas"

' system "cd "+tmpdir$+"X11-Basic/ ; rm -rf smali"

SYSTEM "cd "+tmpdir$+"X11-Basic/smali/net ; mv sourceforge sourceforgm"

projectname$=RIGHT$(basfile$,LEN(basfile$)-rinstr(basfile$,"/"))
projectname$=REPLACE$(projectname$,".","")
s$="net/sourceforge/x11basic"
r$="net/sourceforgm/x11basic"
PRINT "unique string: "+r$
r2$=REPLACE$(r$,"/","_")
s2$=REPLACE$(s$,"/","_")
r3$=REPLACE$(r$,"/",".")
s3$=REPLACE$(s$,"/",".")

t$=system$("cd "+tmpdir$+" ; find . -name '*' ")

WHILE len(t$)
  SPLIT t$,CHR$(10),0,file$,t$
  @replaceinfile(tmpdir$+file$,s$,r$)
  ' @replaceinfile(tmpdir$+file$,s2$,r2$)
  @replaceinfile(tmpdir$+file$,s3$,r3$)

  ' @replaceinfile(tmpdir$+a$,"X11BasicActivity","XXXBasicActivity")

WEND
' replace Icon
@install_icon(tmpdir$+"X11-Basic","../xfel-logo.png")
' Rename project
PRINT projectname$
t$=system$("cd "+tmpdir$+" ; mv X11-Basic "+projectname$)

@replaceinstrings(tmpdir$+"/"+projectname$+"/res/values/strings.xml","app_name","XXX-Basic")
@replaceinstrings(tmpdir$+"/"+projectname$+"/res/values-de/strings.xml","app_name","XXX-Basic")

@replaceinstrings(tmpdir$+"/"+projectname$+"/res/values/strings.xml","app_versionname","XXX-Basic V.0.00")
@replaceinstrings(tmpdir$+"/"+projectname$+"/res/values-de/strings.xml","app_versionname","XXX-Basic V.0.00")

@replaceinstrings(tmpdir$+"/"+projectname$+"/res/values/strings.xml", "copyr","© 2000–2014 Max Mustermann")
@replaceinstrings(tmpdir$+"/"+projectname$+"/res/values-de/strings.xml","copyr","© 2000–2014 Max Mustermann")

@replaceinstrings(tmpdir$+"/"+projectname$+"/res/values/strings.xml", "news","It really works!")
@replaceinstrings(tmpdir$+"/"+projectname$+"/res/values-de/strings.xml","news","Es funktioniert tatsaechlich")

' put together apk file

newapk$=projectname$+".apk"
tmpnewapk$="a"+projectname$+".apk"

SYSTEM "cd "+tmpdir$+" ; apktool b -f "+projectname$+" "+env$("PWD")+"/"+newapk$
' Sign it

SYSTEM "jarsigner -verbose -keystore Android-Keyring -storepass helo -keypass helo2 -digestalg SHA1 -sigalg MD5withRSA -sigfile CERT -signedjar "+tmpnewapk$+" "+newapk$+" drhoffmannsoftware"

' zipalign

SYSTEM "/opt/android-sdk-linux_x86/tools/zipalign -f -v 4 "+tmpnewapk$+" "+newapk$

QUIT

IF qflag=0
  schwarz=COLOR_RGB(0,0,0)
  weiss=COLOR_RGB(1,1,1)
  GET_SCREENSIZE bx,by,bw,bh
  COLOR schwarz
  PBOX bx,by,bx+bw,by+bh
  VSYNC
  COLOR weiss,schwarz
  IF LEN(inputfile$)=0
    TEXT 10,10,"X11-Basic APK-Builder V.1.22 (c) Markus Hoffmann 2011-2014"
    FILESELECT "select program to compile","./*.bas","test.bas",inputfile$
    IF LEN(inputfile$)
      IF NOT EXIST(inputfile$)
        CLR inputfile$
      ENDIF
    ENDIF
  ENDIF
ENDIF
IF UPPER$(RIGHT$(inputfile$,4))<>".BAS" AND UPPER$(RIGHT$(inputfile$,2))<>".B"
  PRINT "File must have the extension: .bas or .b!"
  QUIT
ENDIF
IF LEN(inputfile$)
  t$="[2][You have now following choice:||"
  t$=t$+"1. change the procets name <>,|"
  t$=t$+"2. add a resource file,| "
  t$=t$+"3. change the icon,|"
  t$=t$+"4. modify preferences|"
  t$=t$+"|"

  t$=t$+"][ 1 | 2 | 3 | 4 |finish]"
  IF qflag=0
    COLOR weiss,schwarz
    TEXT 10,32,inputfile$+" OK."
    a=FORM_ALERT(1,t$)
  ELSE
    a=1
  ENDIF
  IF a=1
    @make_bytecode(inputfile$,bfile$)
    @packvm(bfile$)
  ELSE IF a=2
    @make_bytecode(inputfile$,bfile$)
    IF NOT EXIST("xb2c.exe")
      ~FORM_ALERT(1,"[3][xbc: ERROR: xb2c.exe not found.][CANCEL]")
      PRINT "xbc: ERROR: xb2c.exe not found."
      QUIT
    ENDIF
    SYSTEM "xb2c "+bfile$+" -o "+cfile$
    IF exist(cfile$)
      IF qflag=0
        COLOR weiss,schwarz
        TEXT 10,64,bfile$+" --> "+cfile$+" OK."
      ENDIF
      PRINT bfile$+" --> "+cfile$+" OK."
    ENDIF

    @usetcc
  ELSE if a=3
    @make_bytecode(inputfile$,bfile$)
    outputfilename$=bfile$
  ELSE if a=4
    @pseudo
    @usetcc
  ELSE
    QUIT
  ENDIF
  ' Now compilation should have been successful
  IF qflag=0
    IF exist(outputfilename$)
      a=FORM_ALERT(1,"[0][done.| |The program was stored under:|"+outputfilename$+".|Do you want to run it?][RUN|QUIT]")
      IF a=1
        IF outputfilename$=bfile$
          SYSTEM "xbvm "+outputfilename$
        ELSE
          SYSTEM outputfilename$
        ENDIF
      ENDIF
    ELSE
      ~FORM_ALERT(1,"[3][Ups...|compilation was not successful!][ OH ]")
    ENDIF
  ENDIF
ELSE
  IF qflag=0
    ~FORM_ALERT(1,"[2][xbc: No input files.][QUIT]")
  ENDIF
  PRINT "xbc: No input files"
ENDIF
QUIT

PROCEDURE replaceinstrings(filename$,s$,r$)
  LOCAL c$,m$,a$,w1$,w2$,w3$
  PRINT "--> ";filename$
  OPEN "I",#1,filename$
  c$=INPUT$(#1,lof(#1))
  CLOSE #1
  a$="<string name="+CHR$(34)+s$+CHR$(34)+">"
  SPLIT c$,a$,0,w1$,w2$
  IF len(w2$)
    SPLIT w2$,"</string>",0,w2$,w3$
    m$=w1$+a$+r$+"</string>"+w3$
  ELSE
    m$=c$
  ENDIF
  IF m$<>c$
    PRINT filename$+"  (*)"
    BSAVE filename$,VARPTR(m$),LEN(m$)
  ELSE
    ' print "nothing replaced in file. "+filename$
  ENDIF
RETURN

PROCEDURE replaceinfile(filename$,s$,r$)
  LOCAL c$,m$
  PRINT "--> ";filename$
  OPEN "I",#1,filename$
  c$=INPUT$(#1,lof(#1))
  CLOSE #1

  IF filename$="/tmp/apkbuild/./X11-Basic/AndroidManifest.xml"
    MEMDUMP varptr(c$)+64,128
    m$=REPLACE$(c$,s$,r$)
    PRINT s$,r$
    MEMDUMP varptr(c$)+64,128
  ENDIF

  m$=REPLACE$(c$,s$,r$)
  '  m$=@rep$(c$,s$,r$)

  IF instr(m$,s$)
    PRINT "Immernoch da !"
    MEMDUMP varptr(m$)+instr(m$,s$),32
  ENDIF
  IF m$<>c$
    PRINT filename$+"  (*)"
    BSAVE filename$,VARPTR(m$),LEN(m$)
  ELSE
    ' print "nothing replaced in file. "+filename$
  ENDIF
RETURN

' Installs a customized icon into the apk dir
PROCEDURE install_icon(path$,filename$)
  PRINT "ICON --> "+path$
  PRINT "<-- "+filename$
  IF exist(filename$)
    SYSTEM "convert +antialias -density 60 "+filename$+" -antialias -resize 72x72 "+path$+"/res/drawable/x11basic.png"
    SYSTEM "convert +antialias -density 75 "+filename$+" -antialias -adaptive-resize  48x48 "+path$+"/res/drawable-mdpi/x11basic.png"
    SYSTEM "convert +antialias -density 50 "+filename$+" -antialias -liquid-rescale  48x48 "+path$+"/res/drawable-mdpi/x11basic.png"
    SYSTEM "convert +antialias -density 75 "+filename$+" -antialias -liquid-rescale  72x72 "+path$+"/res/drawable-hdpi/x11basic.png"
    SYSTEM "convert +antialias -density 110 "+filename$+" -antialias -resize  96x96 "+path$+"/res/drawable-xhdpi/x11basic.png"
    SYSTEM "convert +antialias -density 180 "+filename$+" -antialias -adaptive-resize  144x144 "+path$+"/res/drawable-xxhdpi/x11basic.png"
  ELSE
    PRINT filename$;" does not exist !"
    QUIT
  ENDIF
RETURN

